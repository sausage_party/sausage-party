/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SMM;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;
/**
 *
 * @author Daniel
 */
public class Login extends javax.swing.JFrame {
    
    SMM koneksi = new SMM();
    private static String user;
    public Login() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PasswordField = new javax.swing.JPasswordField();
        UsernameField = new javax.swing.JTextField();
        Notif = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Login = new javax.swing.JButton();
        Registrasi = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Student Money Manager");
        setBounds(new java.awt.Rectangle(250, 250, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setFocusCycleRoot(false);
        setLocation(new java.awt.Point(380, 140));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        PasswordField.setBackground(new java.awt.Color(0, 0, 0));
        PasswordField.setForeground(new java.awt.Color(255, 255, 255));
        PasswordField.setBorder(null);
        PasswordField.setOpaque(false);
        PasswordField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PasswordFieldActionPerformed(evt);
            }
        });
        getContentPane().add(PasswordField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 150, 120, 20));

        UsernameField.setBackground(new java.awt.Color(0, 0, 0));
        UsernameField.setForeground(new java.awt.Color(255, 255, 255));
        UsernameField.setBorder(null);
        UsernameField.setOpaque(false);
        UsernameField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsernameFieldActionPerformed(evt);
            }
        });
        getContentPane().add(UsernameField, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 104, 110, 20));
        getContentPane().add(Notif, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, 230, 20));

        jLabel5.setIcon(new javax.swing.ImageIcon("C:\\Users\\personal\\Pictures\\LOGIN1.png")); // NOI18N
        jLabel5.setMaximumSize(new java.awt.Dimension(1200, 720));
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 390));

        Login.setText("MASUK");
        Login.setOpaque(false);
        Login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginActionPerformed(evt);
            }
        });
        getContentPane().add(Login, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, 100, 30));

        Registrasi.setText("REGISTRASI");
        Registrasi.setOpaque(false);
        Registrasi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegistrasiActionPerformed(evt);
            }
        });
        getContentPane().add(Registrasi, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 190, 100, 30));

        pack();
    }// </editor-fold>//GEN-END:initComponents

//    private void LoginRegistrasi(){
//
//        
//    }
//    
//    private void Login(){
//       
//    }
    
    
    private void LoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginActionPerformed
        user = UsernameField.getText();
        String pass = PasswordField.getText();
        
        try {
            Connection conn = koneksi.connect();
            PreparedStatement stmt = conn.prepareStatement("Select * from login where username=? and password=?");
            
            stmt.setString(1, user);
            stmt.setString(2, pass);
            
            ResultSet rt = stmt.executeQuery();

            if(user.length() == 0 || pass.length() == 0){
                String notif = ("Username atau Password harus diisi");
                Notif.setText(notif);
            }
            
            else if(rt.next()){      //jika query kosong maka hasilnya false
                showMessageDialog(null, "Login Berhasil"); //untuk mengeluarkan status bar
                new Home().setVisible(true); //untuk menampilkan form baru
                this.setVisible(false); //set form saat ini menjadi invisible
            }
            
            else{
                String notif = ("Anda belum terdaftar, cobalah untuk registrasi");
                Notif.setText(notif);
            }
            rt.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_LoginActionPerformed

    private void UsernameFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsernameFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsernameFieldActionPerformed

    private void PasswordFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PasswordFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PasswordFieldActionPerformed

    private void RegistrasiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegistrasiActionPerformed
        new Registrasi().setVisible(true); //untuk menampilkan form baru
        this.setVisible(false); //set form saat ini menjadi invisible
    }//GEN-LAST:event_RegistrasiActionPerformed

    /**
     * @param args the command line arguments
     */

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() 
            {
                try {
                    Thread.sleep(4500);
                }
                catch(Exception e){
                    
                }
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Login;
    private javax.swing.JLabel Notif;
    private javax.swing.JPasswordField PasswordField;
    private javax.swing.JButton Registrasi;
    private javax.swing.JTextField UsernameField;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables

    
    public String getUser(){
        return user;
    }
}
