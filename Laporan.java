/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SMM;

import java.sql.*;
import java.util.Vector;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;



public class Laporan extends javax.swing.JFrame {
    SMM koneksi = new SMM();
    Connection conn=null;
    ResultSet rs =null;
    Statement st=null;
    String s;
    
    /**
     * Creates new form Laporan
     */
    public Laporan() {
        initComponents();
        update_table_pemasukan();
        update_table_pengeluaran();
        update_table_penyimpanan();
        search_default_state();
    }
    
    private void update_table_pemasukan(){
         
        try{
            conn=koneksi.data();
            st= conn.createStatement();
            s ="Select tanggal,bulan,tahun,nominal from data_pemasukan";
            rs = st.executeQuery(s);
            ResultSetMetaData rsmt = rs.getMetaData();
            
            while(tablePemasukan.getRowCount() > 0) 
            {
                ((DefaultTableModel) tablePemasukan.getModel()).removeRow(0);
            }
            
            int j=1;
            while (rs.next()) {
                //Count the number of column
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                Object[] row = new Object[columns+1];
                row[1 - 1] = j;
                for (int i = 1; i <= columns; i++)
                {  
                    row[i+1 - 1] = rs.getObject(i);
                }
                ((DefaultTableModel) tablePemasukan.getModel()).insertRow(rs.getRow()-1,row);
                j++;
            }     
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
             try{
                 st.close();
                 rs.close();
                 conn.close();
             }catch(Exception e){
                 JOptionPane.showMessageDialog(null, e);
             }
         }
    }
    
    
    private void update_table_pengeluaran(){
          try{
            conn=koneksi.data();
            st= conn.createStatement();
             s ="Select kategori,tanggal,bulan,tahun,nominal from data_pengeluaran";
            rs = st.executeQuery(s);
            ResultSetMetaData rsmt = rs.getMetaData();
            
            while(tablePengeluaran.getRowCount() > 0) 
            {
                ((DefaultTableModel) tablePengeluaran.getModel()).removeRow(0);
            }
            
            int j=1;
            while (rs.next()) {
                //Count the number of column
                
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                Object[] row = new Object[columns+1];
                row[1 - 1] = j;
                for (int i = 1; i <= columns; i++)
                {  
                    row[i+1 - 1] = rs.getObject(i);
                }
                ((DefaultTableModel) tablePengeluaran.getModel()).insertRow(rs.getRow()-1,row);
                j++;
            }     
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
             try{
                 st.close();
                 rs.close();
                 conn.close();
             }catch(Exception e){
                 JOptionPane.showMessageDialog(null, e);
             }
         }
     }
    
     private void update_table_penyimpanan(){
          try{
            conn=koneksi.data();
            st= conn.createStatement();
             s ="Select tanggal,bulan,tahun,nominal from data_penyimpanan";
            rs = st.executeQuery(s);
            ResultSetMetaData rsmt = rs.getMetaData();
            
            while(tablePenyimpanan.getRowCount() > 0) 
            {
                ((DefaultTableModel) tablePenyimpanan.getModel()).removeRow(0);
            }
            int j =1;
            while (rs.next()) {
                //Count the number of column
                
                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                
                Object[] row = new Object[columns+1];
                row[1 - 1] = j;
                for (int i = 1; i <= columns; i++)
                {  
                    row[i+1 - 1] = rs.getObject(i);
                }
                ((DefaultTableModel) tablePenyimpanan.getModel()).insertRow(rs.getRow()-1,row);
                j++;
            }     
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
             try{
                 st.close();
                 rs.close();
                 conn.close();
             }catch(Exception e){
                 JOptionPane.showMessageDialog(null, e);
             }
         }
     }

     
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablePengeluaran = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablePemasukan = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablePenyimpanan = new javax.swing.JTable();
        Kembali2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        S_Bulan = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        S_Tabel = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        S_Search = new javax.swing.JButton();
        S_Kategori = new javax.swing.JComboBox();
        S_Tanggal = new javax.swing.JComboBox();
        SL_Tahun = new javax.swing.JLabel();
        S_Tahun = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Laporan");
        setLocation(new java.awt.Point(150, 100));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablePengeluaran.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "No", "Kategori", "Tanggal", "Bulan", "Tahun", "Nominal"
            }
        ));
        jScrollPane1.setViewportView(tablePengeluaran);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 168, 410, 279));

        tablePemasukan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "No", "Tanggal", "Bulan", "Tahun", "Nominal"
            }
        ));
        jScrollPane2.setViewportView(tablePemasukan);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(28, 167, 374, 280));

        tablePenyimpanan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "No", "Tanggal", "Bulan", "Tahun", "Nominal"
            }
        ));
        jScrollPane3.setViewportView(tablePenyimpanan);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(824, 167, 342, 280));

        Kembali2.setText("KEMBALI");
        Kembali2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Kembali2ActionPerformed(evt);
            }
        });
        getContentPane().add(Kembali2, new org.netbeans.lib.awtextra.AbsoluteConstraints(28, 27, -1, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/SMM/Capture5.PNG"))); // NOI18N
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(-6, -6, 1190, 470));

        S_Bulan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<ANY>", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" }));
        S_Bulan.setToolTipText("");
        getContentPane().add(S_Bulan, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 590, 130, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Tabel    ");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, -1, -1));

        S_Tabel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<Pilih Tabel>", "Pemasukan", "Pengeluaran", "Penyimpanan" }));
        S_Tabel.setToolTipText("");
        S_Tabel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                S_TabelActionPerformed(evt);
            }
        });
        getContentPane().add(S_Tabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 470, 130, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Tahun");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 630, 50, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Tanggal");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 550, 60, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Kategori");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 510, 60, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Bulan");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 590, 50, -1));

        jLabel1.setText(":");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 510, 20, -1));

        jLabel8.setText(":");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 470, 20, -1));

        jLabel9.setText(":");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 630, 20, -1));

        jLabel10.setText(":");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 550, 20, -1));

        jLabel11.setText(":");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 590, 20, -1));

        S_Search.setText("Search");
        S_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                S_SearchActionPerformed(evt);
            }
        });
        getContentPane().add(S_Search, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 680, 220, -1));

        S_Kategori.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<ANY>", "Konsumsi", "Transportasi", "Sewa Rumah", "Olahraga", "Laundry", "Kegiatan Kampus", "Pulsa", "Kesehatan", "Shopping", "Internet", "Dan Lain-lain" }));
        S_Kategori.setToolTipText("");
        getContentPane().add(S_Kategori, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 510, 130, -1));

        S_Tanggal.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<ANY>", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        S_Tanggal.setToolTipText("");
        getContentPane().add(S_Tanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 550, 130, -1));
        getContentPane().add(SL_Tahun, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 630, 160, 20));

        S_Tahun.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<ANY>" }));
        S_Tahun.setToolTipText("");
        getContentPane().add(S_Tahun, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 630, 130, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Kembali2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Kembali2ActionPerformed
        new Home().setVisible(true); //untuk menampilkan form baru
        this.setVisible(false); //set form saat ini menjadi invisible
    }//GEN-LAST:event_Kembali2ActionPerformed

    
    
//=============================================================================   
//------------------------------PENCARIAN------------------------------------- 
    
    private void isi_tahun(int i){
        S_Tahun.removeAllItems();
        S_Tahun.addItem("<ANY>");
        try(Connection conn = koneksi.data()){
            String tabel;
            if(i==1){
                tabel = "data_pemasukan";
            }
            else if(i==2){
                tabel = "data_pengeluaran";
            }
            else{
                tabel = "data_penyimpanan";
            }
            PreparedStatement select = conn.prepareStatement("Select distinct tahun from "+tabel+"");
            ResultSet rs_select = select.executeQuery();
            
            while (rs_select.next()) {
                
                String tahun_db = rs_select.getString("tahun");
                S_Tahun.addItem(tahun_db);

            }
            
            // Tutup koneksi
            rs_select.close();
            select.close();
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    private void search_default_state(){
        S_Kategori.setEnabled(false);
        S_Tanggal.setEnabled(false);
        S_Bulan.setEnabled(false);
        S_Tahun.setEnabled(false);
        S_Search.setEnabled(false);
        SL_Tahun.setText("");
    }
    
    private void S_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_S_SearchActionPerformed
        
        String tabel = S_Tabel.getSelectedItem().toString();
        if(tabel == "Pemasukan"){
            S_Pemasukan();
        }
        else if(tabel == "Pengeluaran"){
            S_Pengeluaran();
        }
        else if(tabel == "Penyimpanan"){
            S_Penyimpanan();
        }
        
        
        
        
           
    }//GEN-LAST:event_S_SearchActionPerformed

    
    private void S_Pemasukan(){
        
        String tanggal = "not null";
        String bulan = "not null";
        String tahun = "not null";
        
        if(S_Tanggal.getSelectedItem().toString()!="<ANY>"){
            tanggal = S_Tanggal.getSelectedItem().toString();
        }
        if(S_Bulan.getSelectedItem().toString()!="<ANY>"){
            bulan = "'"+S_Bulan.getSelectedItem().toString()+"'";
        }
        if(S_Tahun.getSelectedItem().toString()!="<ANY>"){
            tahun = S_Tahun.getSelectedItem().toString();
        }
        
        
            try{
            conn=koneksi.data();
            PreparedStatement search = conn.prepareStatement("Select tanggal,bulan,tahun,nominal from data_pemasukan where tanggal is "+tanggal+" and bulan is "+bulan+" and tahun is "+tahun+"");
            ResultSet rs = search.executeQuery();
            while(tablePemasukan.getRowCount() > 0) 
            {
                ((DefaultTableModel) tablePemasukan.getModel()).removeRow(0);
            }
            int j=1;
            while (rs.next()) {
                //Count the number of column

                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                Object[] row = new Object[columns+1];
                row[1 - 1] = j;
                for (int i = 1; i <= columns; i++)
                {  
                    row[i+1 - 1] = rs.getObject(i);
                }
                ((DefaultTableModel) tablePemasukan.getModel()).insertRow(rs.getRow()-1,row);
                j++;

            }     
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
             try{
                 rs.close();
                 conn.close();
             }catch(Exception e){
                 JOptionPane.showMessageDialog(null, e);
             }
        }
        
    }
  
    private void S_Pengeluaran(){
        
        String tanggal = "not null";
        String bulan = "not null";
        String tahun = "not null";
        String kategori = "not null";
        
        if(S_Tanggal.getSelectedItem().toString()!="<ANY>"){
            tanggal = S_Tanggal.getSelectedItem().toString();
        }
        if(S_Bulan.getSelectedItem().toString()!="<ANY>"){
            bulan = "'"+S_Bulan.getSelectedItem().toString()+"'";
        }
        if(S_Tahun.getSelectedItem().toString()!="<ANY>"){
            tahun = S_Tahun.getSelectedItem().toString();
        }
        if(S_Kategori.getSelectedItem().toString()!="<ANY>"){
            kategori = "'"+S_Kategori.getSelectedItem().toString()+"'";
        }
        
        
            try{
            conn=koneksi.data();
            PreparedStatement search = conn.prepareStatement("Select kategori,tanggal,bulan,tahun,nominal from data_pengeluaran where kategori is "+kategori+" and tanggal is "+tanggal+" and bulan is "+bulan+" and tahun is "+tahun+"");
            ResultSet rs = search.executeQuery();
            while(tablePengeluaran.getRowCount() > 0) 
            {
                ((DefaultTableModel) tablePengeluaran.getModel()).removeRow(0);
            }
            int j=1;
            while (rs.next()) {
                //Count the number of column

                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                Object[] row = new Object[columns+1];
                row[1 - 1] = j;
                for (int i = 1; i <= columns; i++)
                {  
                    row[i+1 - 1] = rs.getObject(i);
                }
                ((DefaultTableModel) tablePengeluaran.getModel()).insertRow(rs.getRow()-1,row);
                j++;

            }     
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
             try{
                 rs.close();
                 conn.close();
             }catch(Exception e){
                 JOptionPane.showMessageDialog(null, e);
             }
        }
        
    }
    
    private void S_Penyimpanan(){
        
        String tanggal = "not null";
        String bulan = "not null";
        String tahun = "not null";
        
        if(S_Tanggal.getSelectedItem().toString()!="<ANY>"){
            tanggal = S_Tanggal.getSelectedItem().toString();
        }
        if(S_Bulan.getSelectedItem().toString()!="<ANY>"){
            bulan = "'"+S_Bulan.getSelectedItem().toString()+"'";
        }
        if(S_Tahun.getSelectedItem().toString()!="<ANY>"){
            tahun = S_Tahun.getSelectedItem().toString();
        }
        
        
            try{
            conn=koneksi.data();
            PreparedStatement search = conn.prepareStatement("Select tanggal,bulan,tahun,nominal from data_penyimpanan where tanggal is "+tanggal+" and bulan is "+bulan+" and tahun is "+tahun+"");
            ResultSet rs = search.executeQuery();
            while(tablePenyimpanan.getRowCount() > 0) 
            {
                ((DefaultTableModel) tablePenyimpanan.getModel()).removeRow(0);
            }
            int j=1;
            while (rs.next()) {
                //Count the number of column

                ResultSetMetaData md = rs.getMetaData();
                int columns = md.getColumnCount();
                Object[] row = new Object[columns+1];
                row[1 - 1] = j;
                for (int i = 1; i <= columns; i++)
                {  
                    row[i+1 - 1] = rs.getObject(i);
                }
                ((DefaultTableModel) tablePenyimpanan.getModel()).insertRow(rs.getRow()-1,row);
                j++;

            }     
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }finally{
             try{
                 rs.close();
                 conn.close();
             }catch(Exception e){
                 JOptionPane.showMessageDialog(null, e);
             }
        }
        
    }
    
    
    private void S_TabelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_S_TabelActionPerformed
        String tabel = S_Tabel.getSelectedItem().toString();
        int i;
        if(tabel == "Pemasukan"){
            i=1;
            S_Kategori.setEnabled(false);
            S_Tanggal.setEnabled(true);
            S_Bulan.setEnabled(true);
            S_Tahun.setEnabled(true);
            S_Search.setEnabled(true);
            S_Kategori.setSelectedIndex(0);
            S_Tanggal.setSelectedIndex(0);
            S_Bulan.setSelectedIndex(0);
            S_Tahun.setSelectedIndex(0);
            isi_tahun(i);
        }
        else if(tabel == "Pengeluaran"){
            i=2;
            S_Kategori.setEnabled(true);
            S_Tanggal.setEnabled(true);
            S_Bulan.setEnabled(true);
            S_Tahun.setEnabled(true);
            S_Search.setEnabled(true);
            S_Kategori.setSelectedIndex(0);
            S_Tanggal.setSelectedIndex(0);
            S_Bulan.setSelectedIndex(0);
            S_Tahun.setSelectedIndex(0);
            isi_tahun(i);
        }
        else if(tabel == "Penyimpanan"){
            i=3;
            S_Kategori.setEnabled(false);
            S_Tanggal.setEnabled(true);
            S_Bulan.setEnabled(true);
            S_Tahun.setEnabled(true);
            S_Search.setEnabled(true);
            S_Kategori.setSelectedIndex(0);
            S_Bulan.setSelectedIndex(0);
            S_Tahun.setSelectedIndex(0);
            isi_tahun(i);
        }
        else{
            search_default_state();
        }
        
        
    }//GEN-LAST:event_S_TabelActionPerformed

    
    
    
    
    
//-------------------------------------------------------------------------------
//===============================================================================
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Laporan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Laporan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Kembali2;
    private javax.swing.JLabel SL_Tahun;
    private javax.swing.JComboBox S_Bulan;
    private javax.swing.JComboBox S_Kategori;
    private javax.swing.JButton S_Search;
    private javax.swing.JComboBox S_Tabel;
    private javax.swing.JComboBox S_Tahun;
    private javax.swing.JComboBox S_Tanggal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tablePemasukan;
    private javax.swing.JTable tablePengeluaran;
    private javax.swing.JTable tablePenyimpanan;
    // End of variables declaration//GEN-END:variables
}
